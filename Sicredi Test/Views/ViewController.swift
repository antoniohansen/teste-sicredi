//
//  ViewController.swift
//  Sicredi Test
//
//  Created by Antonio Carlos Hansen Filho on 2/6/19.
//  Copyright © 2019 Antonio Carlos Hansen Filho. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    var eventData: [Event] = []
    
    let mainUrl = "https://5b840ba5db24a100142dcd8c.mockapi.io/api/"
    
    override func viewWillAppear(_ animated: Bool) {
        fetchEvents()
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //Busca os eventos da API
    func fetchEvents() {
        guard let url = URL(string: mainUrl + "events") else { return }
        
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else { return }
            do {
                let events = try JSONDecoder().decode([Event].self, from: data)
                self.eventData = events
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            } catch let jsonErr {
                print(jsonErr)
            }
        }.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell")
        cell?.textLabel?.text = eventData[indexPath.row].title
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ViewControllerDetail {
            destination.eventDetail = self.eventData[(self.tableView.indexPathForSelectedRow?.row)!]
            tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
        }
    }
}

