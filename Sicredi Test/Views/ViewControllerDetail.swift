//
//  ViewControllerDetail.swift
//  Sicredi Test
//
//  Created by Antonio Carlos Hansen Filho on 3/6/19.
//  Copyright © 2019 Antonio Carlos Hansen Filho. All rights reserved.
//

import UIKit

class ViewControllerDetail: UIViewController {
    
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventDescription: UILabel!
    
    var eventDetail: Event?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventTitle.text = eventDetail?.title
        eventDescription.text = eventDetail?.description
    }
}
