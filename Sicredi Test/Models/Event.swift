//
//  Event.swift
//  Sicredi Test
//
//  Created by Antonio Carlos Hansen Filho on 2/6/19.
//  Copyright © 2019 Antonio Carlos Hansen Filho. All rights reserved.
//

import Foundation

class Event: Codable {
    let id: String?
    let title: String?
    let price: Double?
    let latitude, longitude: String?
    let image: String?
    let description: String?
    let date: Date?
    let rsvps: [Rsvp]?
    let cupons: [Coupon]?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id", title = "title", price = "price", latitude = "latitude", longitude = "longitude", image = "image", description = "description", date = "date", rsvps = "people", cupons = "cupons"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.price = try container.decode(Double.self, forKey: .price)
        
        if let latitude = try? container.decode(Double.self, forKey: .latitude) {
            self.latitude = String(latitude)
        } else {
            self.latitude = try container.decode(String.self, forKey: .latitude)
        }
        
        if let longitude = try? container.decode(Double.self, forKey: .longitude) {
            self.longitude = String(longitude)
        } else {
            self.longitude = try container.decode(String.self, forKey: .longitude)
        }
        
        self.image = try container.decode(String.self, forKey: .image)
        self.description = try container.decode(String.self, forKey: .description)
        
        let dateTime = try container.decode(Double.self, forKey: .date)
        self.date = Date(timeIntervalSinceReferenceDate: dateTime)
        
        self.rsvps = try container.decode([Rsvp].self, forKey: .rsvps)
        self.cupons = try container.decode([Coupon].self, forKey: .cupons)
    }

}
