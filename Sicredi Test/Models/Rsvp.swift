//
//  Rsvp.swift
//  Sicredi Test
//
//  Created by Antonio Carlos Hansen Filho on 2/6/19.
//  Copyright © 2019 Antonio Carlos Hansen Filho. All rights reserved.
//

import Foundation

class Rsvp: Codable {
    let id: String?
    let eventId, name: String?
    let picture: String?
    
    init(id: String, eventId: String, name: String, picture: String) {
        self.id = id
        self.eventId = eventId
        self.name = name
        self.picture = picture
    }
}
