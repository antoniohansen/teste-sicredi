//
//  Coupon.swift
//  Sicredi Test
//
//  Created by Antonio Carlos Hansen Filho on 2/6/19.
//  Copyright © 2019 Antonio Carlos Hansen Filho. All rights reserved.
//

import Foundation

class Coupon: Codable {
    let id: String?
    let eventId: String?
    let discount: Decimal?
    
    init(id: String, eventId: String, discount: Decimal) {
        self.id = id
        self.eventId = eventId
        self.discount = discount
    }
}
